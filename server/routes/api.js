const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Video = require('../models/video'); 

const db = "mongodb://django:metrolife1@ds135421.mlab.com:35421/my_videoplayer";
mongoose.Promise = global.Promise;

mongoose.connect(db, (err) => {
	if (err) console.log(err);
});

router.get('/', (req, res) => {
	res.send('api works!');
});

router.get('/videos', (req, res) => {
	console.log('Get request for all videos');
	Video.find({}).exec((err, videos) => {
		if (err) {
			console.log('Error Occured!');
		} else {
			res.json(videos);	
		} 
	});	
});

router.get('/video/:id', (req, res) => {
	console.log('Get request for a single video!');
	Video.findById(req.params.id).exec((err, video) => {
		if (err) {
			console.log('Error Occured!');
		} else {
			res.json(video);	
		} 
	});	
});

router.post('/video', (req, res) => {
	console.log('Post (save) a video!');
	var newVideo = new Video();
	newVideo.title = req.body.title;
	newVideo.url = req.body.url;
	newVideo.description = req.body.description;
	newVideo.save((err, insertedVideo) => {
		if (err) {
			console.log('Error saving video');
		} else {
			res.json(insertedVideo);
		}
	});
});

router.put('/video/:id', (req, res) => {
	console.log('Update a video!');
	Video.findByIdAndUpdate(
		req.params.id,
		{
			$set: {
				title: req.body.title, 
				url: req.body.url,
				description: req.body.description
			}
		},
		{
			new: true
		},
		(err, updatedVideo) => {
			if (err) {
				res.send("Error while updating video1");
			} else {
				res.json(updatedVideo);
			}
		}
	);
});

router.delete('/video/:id', (req, res) => {
	console.log('Deleting a video...');
	Video.findByIdAndDelete(req.params.id, (err, deletedVideo) => {
		if (err) {
			console.log(err);
		} else {
			res.json(deletedVideo);
		}
	});
});

module.exports = router;